const mongoose = require('mongoose');
const Schema = mongoose.Schema;
require('mongoose-currency').loadType(mongoose);

const favoriteSchema = new Schema({
    user:  {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    dishes: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Dish',
    }]
}, {
    timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' }
});

var Favorites = mongoose.model('Favorite', favoriteSchema);

module.exports = Favorites;